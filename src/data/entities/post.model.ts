import { Column, Entity } from 'typeorm';
import { BaseEntity } from './base.model';

@Entity('post')
export class Posts extends BaseEntity {
  @Column()
  title: string;

  @Column()
  description: string;
}
