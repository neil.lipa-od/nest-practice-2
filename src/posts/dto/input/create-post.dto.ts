import { IsNotEmpty, Matches } from 'class-validator';

export class CreatePostDto {
  @IsNotEmpty()
  @Matches(/[a-zA-Z]+/g, { message: 'Title should only be letters only' })
  title: string;

  @IsNotEmpty()
  description: string;
}
