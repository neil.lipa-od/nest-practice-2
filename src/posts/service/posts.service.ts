import { Injectable } from '@nestjs/common';
import { Posts } from 'src/data/entities/post.model';
import { CreatePostDto } from '../dto/input/create-post.dto';
import { UpdatePostDto } from '../dto/input/update-post.dto';
import { PostRepository } from '../repository/post.repository';

@Injectable()
export class PostsService {
  constructor(private postRepository: PostRepository) {}

  async create(createPostDto: CreatePostDto): Promise<Posts> {
    return await this.postRepository.createPost(createPostDto);
  }

  async findAll(): Promise<Posts[]> {
    return this.postRepository.findAllPosts();
  }

  async findOne(id: string) {
    return this.postRepository.findOnePost(id);
  }

  async update(id: string, updatePostDto: UpdatePostDto) {
    return this.postRepository.updatePost(id, updatePostDto);
  }

  async remove(id: string) {
    return this.postRepository.deletePost(id);
  }
}
