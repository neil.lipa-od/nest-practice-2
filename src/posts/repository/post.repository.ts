import { Posts } from 'src/data/entities/post.model';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(Posts)
export class PostRepository extends Repository<Posts> {
  async createPost(post: Posts) {
    return super.save(post);
  }

  async findAllPosts(): Promise<Posts[]> {
    return super.find();
  }

  async findOnePost(id: string): Promise<Posts> {
    return super.findOne(id);
  }

  async updatePost(id: string, post: Partial<Posts>) {
    return super.update(id, post);
  }

  async deletePost(id: string) {
    return super.delete(id);
  }
}
