import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CreatePostDto } from '../dto/input/create-post.dto';
import { UpdatePostDto } from '../dto/input/update-post.dto';
import { PostsService } from '../service/posts.service';

@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Post()
  create(@Body() createPostDto: CreatePostDto) {
    return this.postsService.create(createPostDto);
  }

  @Get()
  findAll() {
    return this.postsService.findAll();
  }

  @Get(':postId')
  findOne(@Param('postId') id: string) {
    return this.postsService.findOne(id);
  }

  @Patch(':postId')
  update(@Param('postId') id: string, @Body() updatePostDto: UpdatePostDto) {
    return this.postsService.update(id, updatePostDto);
  }

  @Delete(':postId')
  remove(@Param('postId') id: string) {
    return this.postsService.remove(id);
  }
}
