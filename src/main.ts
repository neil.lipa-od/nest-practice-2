import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  await app.listen(parseInt(process.env.APP_PORT) || 3000).then(async () => {
    console.log(`Application is running on: ${await app.getUrl()}`);
  });
}
bootstrap();
